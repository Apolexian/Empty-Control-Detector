# Java empty control flow and recursion detection

Project to find empty control flow statements and recursive functions made as part of a university course assessment

## Running the project

Run the project through the generated jar.
Open a terminal and change directories to where the jar is located.
Run the jar.

```shell
workspace$: cd workshop3
workspace$: java -jar workshop3.jar

```

The program will prompt for input to determine the location of the file to analyse.
For example to analyse the Calculator.java file provided with the project enter its path
on the line after the prompt:

```shell
Enter file path to analyse
java/calculator/Calculator.java

```

The program will analyse the code and produce output in the std-out, for example:

```shell
Useless control flows:
Breakpoint [className=Calculator, methodName=null, lineStart=12, lineEnd=12]
Breakpoint [className=Calculator, methodName=divide, lineStart=31, lineEnd=33]
Breakpoint [className=Calculator, methodName=method1, lineStart=106, lineEnd=108]
Polymorphic recursions:
Breakpoint [className=Calculator, methodName=multiply2, lineStart=51, lineEnd=61]
Breakpoint [className=Calculator, methodName=method1, lineStart=101, lineEnd=104]
```

The output is structured as so:

* className - the name of the class that contains the found useless control flow or
polymorphic recursion.
* methodName - the name of the method that contains the found useless control flow or
polymorphic recursion (in the case that method name does not apply, this will default
to null).
* lineStart - the starting line of the found useless control flow or polymorphic recursion.
* lineEnd - the ending line of the found useless control flow or polymorphic recursion.

In order to analyse another file run the program again and input the path of the new file.

## Dependencies

Maven dependencies:

* com.github.javaparser
* org.jetbrains (annotations)

See pom.xml for more info.
