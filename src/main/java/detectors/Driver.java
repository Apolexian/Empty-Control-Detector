package detectors;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ParseProblemException;
import com.github.javaparser.ast.CompilationUnit;

/**
 * @author Ivan Nikitin
 */
public class Driver {

    private static Scanner reader = new Scanner(System.in);

    public static void main(String[] args) {
        FileInputStream inputStream = null;
        String path;

        // prompt user input for file
        System.out.println("Enter file path to analyse");
        path = reader.nextLine();

        try {
            // start input stream of file
            inputStream = new FileInputStream(path);
            CompilationUnit cu = JavaParser.parse(inputStream);

            // visitors declaration
            UselessControlFlowDetector visitor = new UselessControlFlowDetector();
            RecursionDetector recurVisitor = new RecursionDetector();

            // breakpoint arrays declaration
            List<Breakpoints> controller = new ArrayList<>();
            List<Breakpoints> recurCollector = new ArrayList<>();

            // visit
            visitor.visit(cu, controller);
            recurVisitor.visit(cu, recurCollector);

            // print results of useless control flow
            System.out.println("Useless control flows:");
            for (Breakpoints b : controller)
                System.out.println(b);

            // print result of polymorphic recursion
            System.out.println("Polymorphic recursions:");
            for (Breakpoints b : recurCollector)
                System.out.println(b);


        } catch (FileNotFoundException | ParseProblemException e) {
            System.out.println("Could not find file or parser problem.");
            e.printStackTrace();
        } finally {
            try {
                assert inputStream != null;
                inputStream.close();
            } catch (IOException e) {
                System.out.println("Could not close input stream.");
                e.printStackTrace();
            }
        }
    }
}