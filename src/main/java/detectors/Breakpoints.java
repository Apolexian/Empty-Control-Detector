package detectors;

import java.util.Objects;

/**
 * @author Ivan Nikitin
 */
public class Breakpoints {
    private String className;
    private String methodName;
    private int lineStart, lineEnd;

    /**
     * @param className  - name of class where breakpoint established
     * @param methodName - name of method where breakpoint established
     * @param lineStart  - start line of breakpoint
     * @param lineEnd    - end line of breakpoint
     */
    public Breakpoints(String className, String methodName, int lineStart, int lineEnd) {
        this.className = className;
        this.methodName = methodName;
        this.lineStart = lineStart;
        this.lineEnd = lineEnd;
    }

    @Override
    public String toString() {
        return "Breakpoint [className=" + className + ", methodName=" + methodName + ", lineStart=" + lineStart
                + ", lineEnd=" + lineEnd + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Breakpoints that = (Breakpoints) o;
        return lineStart == that.lineStart &&
                lineEnd == that.lineEnd &&
                Objects.equals(className, that.className) &&
                Objects.equals(methodName, that.methodName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(className, methodName, lineStart, lineEnd);
    }
}
