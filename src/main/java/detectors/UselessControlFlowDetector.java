package detectors;

import java.util.List;
import java.util.Objects;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.EmptyStmt;
import com.github.javaparser.ast.stmt.SwitchEntry;
import com.github.javaparser.ast.stmt.SwitchStmt;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import org.jetbrains.annotations.NotNull;

/**
 * @author Ivan Nikitin
 */
public class UselessControlFlowDetector extends VoidVisitorAdapter<List<Breakpoints>> {
    private String className;
    private String methodName;
    private List<Breakpoints> collector;

    /**
     * create new breakpoint - check if breakpoint already exists in collector
     * e.g block can overlap with method check and cause over counting
     * hence add only unique elements
     *
     * @param nodeTarget - node for which breakpoint is being made
     *                   e.g block or method declaration
     */
    public void createBreakPoint(@NotNull Node nodeTarget) {
        Breakpoints newBreakpoint = new Breakpoints(className, methodName, Objects.requireNonNull(
                nodeTarget.getRange().orElse(null)).begin.line, nodeTarget.getRange().get().end.line);

        for (Breakpoints breakpoint : collector) {
            if (breakpoint.equals(newBreakpoint)) {
                return;
            }
        }
        this.collector.add(newBreakpoint);
    }

    /**
     * detect class or interface (to get name and collector)
     *
     * @param classOrInterface - class or interface declaration to get name
     * @param collector        - list of breakpoints (the breakpoint collector)
     */
    @Override
    public void visit(@NotNull ClassOrInterfaceDeclaration classOrInterface, List<Breakpoints> collector) {

        this.className = classOrInterface.getName().asString();
        this.collector = collector;
        super.visit(classOrInterface, collector);
    }

    /**
     * detect method declaration and visit child nodes
     *
     * @param method    - method declaration
     * @param collector - list of breakpoints (the breakpoint collector)
     */
    @Override
    public void visit(@NotNull MethodDeclaration method, List<Breakpoints> collector) {
        this.methodName = method.getName().asString();
        super.visit(method, collector);
        if (Objects.requireNonNull(method.getBody().orElse(null)).isEmpty()) {
            createBreakPoint(method);
        }
    }

    /**
     * detect empty constructors
     *
     * @param constructor - constructor declaration
     * @param collector   -  list of breakpoints (the breakpoint collector)
     */
    @Override
    public void visit(ConstructorDeclaration constructor, List<Breakpoints> collector) {
        super.visit(constructor, collector);
        if (constructor.getBody().isEmpty())
            createBreakPoint(constructor);
    }

    /**
     * detect empty block statements (will also cause methods to double print)
     * this is explained and dealt with in the createBreakPoint method
     *
     * @param block     - block statement (i.e anything between {})
     * @param collector - list of breakpoints (the breakpoint collector)
     */
    @Override
    public void visit(BlockStmt block, List<Breakpoints> collector) {
        super.visit(block, collector);
        if (block.isEmpty()) {
            createBreakPoint(block);
        }
    }

    /**
     * check for empty statements
     * @param block - EmptyStmt block
     * @param collector - list of breakpoints
     */
    @Override
    public void visit(EmptyStmt block, List<Breakpoints> collector) {
        super.visit(block, collector);
        if (block.isEmptyStmt()) {
            createBreakPoint(block);
        }
    }

    /**
     * detect empty switch statement
     *
     * @param switchCase - switch case declaration
     * @param collector  - list of breakpoints (the breakpoint collector)
     */
    @Override
    public void visit(SwitchStmt switchCase, List<Breakpoints> collector) {
        super.visit(switchCase, collector);
        NodeList<SwitchEntry> entries = switchCase.getEntries();
        if (entries.isEmpty())
            createBreakPoint(switchCase);
    }
}