package detectors;

import java.util.List;
import java.util.Objects;
import java.util.Stack;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import org.jetbrains.annotations.NotNull;

/**
 * @author Ivan Nikitin
 */
public class RecursionDetector extends VoidVisitorAdapter<List<Breakpoints>> {
    private String methodName, className;
    private Stack<MethodDeclaration> methodStack = new Stack<>();
    private List<Breakpoints> collector;
    private boolean recursive = false;

    /**
     * create new breakpoint - check if breakpoint already exists in collector
     * e.g block can overlap with method check and cause over counting
     * hence add only unique elements
     *
     * @param nodeTarget - node for which breakpoint is being made
     *                   e.g block or method declaration
     */
    public void createBreakPoint(@NotNull Node nodeTarget) {
        Breakpoints newBreakpoint = new Breakpoints(className, methodName, Objects.requireNonNull(
                nodeTarget.getRange().orElse(null)).begin.line, nodeTarget.getRange().get().end.line);

        for (Breakpoints breakpoint : collector) {
            if (breakpoint.equals(newBreakpoint)) {
                return;
            }
        }
        this.collector.add(newBreakpoint);
    }

    /**
     * detect class or interface (to get name and collector)
     *
     * @param classOrInterface - class or interface declaration to get name
     * @param collector        - list of breakpoints (the breakpoint collector)
     */
    @Override
    public void visit(@NotNull ClassOrInterfaceDeclaration classOrInterface, List<Breakpoints> collector) {
        this.className = classOrInterface.getName().asString();
        this.collector = collector;
        super.visit(classOrInterface, collector);
    }

    /**
     * detect method declaration and visit child nodes
     *
     * @param method    - method declaration
     * @param collector - list of breakpoints (the breakpoint collector)
     */
    @Override
    public void visit(MethodDeclaration method, List<Breakpoints> collector) {
        methodStack.push(method);
        super.visit(method, collector);
        if (recursive) {
            this.methodName = method.getName().asString();
            createBreakPoint(methodStack.pop());
            recursive = false;
        }
    }

    /**
     * if method has same name as declaration then the method
     * on stack top is recursive
     *
     * @param methodCall - method call
     * @param collector  - list of breakpoints (the breakpoint collector)
     */
    @Override
    public void visit(@NotNull MethodCallExpr methodCall, List<Breakpoints> collector) {
        String methodCallName = methodStack.peek().getName().asString();
        if (methodCall.getName().asString().equals(methodCallName)) {
            recursive = true;
        }
    }
}
